import envVariables
import json
import requests
from requests_oauthlib import OAuth1

# TODO: make this not specific to one account. go through the authorisation process to verify an account then use that account
headerOauth = OAuth1(envVariables.OAUTH_CONSUMER_KEY,
					 envVariables.OAUTH_CONSUMER_SECRET,
                     envVariables.OAUTH_TOKEN,
					 envVariables.OAUTH_TOKEN_SECRET,
                     signature_type='auth_header')

urlPostTweet = "https://api.twitter.com/1.1/statuses/update.json"

class TweetError(Exception):
	def __init__(self, errors):
		self.errors = errors
	
	def __str__(self):
		errorString = "Tweet failed. Twitter API error(s):\n"
		for error in self.errors:
			errorString = errorString + "\t" + str(error["code"]) + ": " + error["message"] + "\n"
		return errorString

def postTweet(tweetText):
	queryString = {"status":tweetText}
	response = requests.request("POST", urlPostTweet, auth=headerOauth, params=queryString)
	return getTweetData(response)

def replyToTweet(tweetText, tweetDataToReplyTo, tweetDataToQuote = None):

	if tweetDataToQuote:
		queryString = {	"status" : tweetText,
						"attachment_url" : getTweetLink(tweetDataToQuote),
						"in_reply_to_status_id" : tweetDataToReplyTo["idStr"],
						"auto_populate_reply_metadata" : True
						}
	else:
		queryString = {	"status" : tweetText,
						"in_reply_to_status_id" : tweetDataToReplyTo["idStr"],
						"auto_populate_reply_metadata" : True
						}
						
	response = requests.request("POST", urlPostTweet, auth=headerOauth, params=queryString)
	return getTweetData(response)

def getTweetLink(tweetData):
	return "https://twitter.com/" + tweetData["userStr"] + "/status/" + tweetData["idStr"]

def deleteTweet(tweetData):
	deleteRequestUrl = "https://api.twitter.com/1.1/statuses/destroy/" + tweetData["idStr"] + ".json"
	response = requests.request("POST", deleteRequestUrl, auth=headerOauth)
	return getTweetData(response)

def getTweetData(response):
	#TODO: Special cases for error codes such as rate count reached, character limit reached
	responseCode = response.status_code
	responsePy = json.loads(response.text)
	
	if responseCode == requests.codes.ok:
		return {	"idStr": responsePy["id_str"],
					"userStr": responsePy["user"]["screen_name"]
					}
	else:
		raise TweetError(responsePy["errors"])
		
	return None