## twine parsing script

import html
import re

# class serving the purpose of an enum. is the passage text a choice, that links to another passage, or regular passage text
class passageTextTypeClass:
	def __init__(self):
		self.passageText = 0
		self.choiceText = 1

class twineParser:
	
	def __init__(self, twineHtmlFilename):
		self.twineHtmlFilename = twineHtmlFilename
		self.passageTextType = passageTextTypeClass()
	
	# reads in file and returns string covered by the <tw-storydata> tags
	def importTwineHtmlFile(self):
		twineHtmlFile = open(self.twineHtmlFilename, "r")
		
		htmlFileLines = twineHtmlFile.readlines()
		linesOfInterest = list()

		lineIsOfInterest = False
		for line in htmlFileLines:
			if line.startswith("<tw-storydata"):
				lineIsOfInterest = True
			
			if lineIsOfInterest:
				linesOfInterest.append(line)

			if line.endswith("</tw-storydata>\n"):
				break

		storydataHtml = html.unescape("".join(linesOfInterest))
		return storydataHtml

	def getStartNode(self, storydataDivHeadingsStr):
		return int(self.getValueFromHtmlId("startnode", storydataDivHeadingsStr))

	def getPID(self, passageDataDivHeadingsStr):
		return int(self.getValueFromHtmlId("pid", passageDataDivHeadingsStr))

	def getPassageName(self, passageDataDivHeadingsStr):
		return self.getValueFromHtmlId("name", passageDataDivHeadingsStr)

	def getTags(self, passageDataDivHeadingsStr):
		tagsStr = self.getValueFromHtmlId("tags", passageDataDivHeadingsStr)
		return tagsStr.split()

	# look for e.g. id="value", return value string
	def getValueFromHtmlId(self, id, stringToSearch):
		patternStr = id + "=\".*?\""
		pattern = re.compile(patternStr)
		foundStr = pattern.search(stringToSearch).group()
		return foundStr[(len(id)+2):(len(foundStr)-1)]

	def getContentData(self, contentStr):
		choicesPattern = re.compile("(\[\[.*?\]\])")
		contentSubStrings = choicesPattern.split(contentStr)

		contentData = list()
		for contentSubString in contentSubStrings:
			# strip - we don't want whitespace at the start/end of tweets
			contentSubString = contentSubString.strip()
			
			if contentSubString:
			
				if choicesPattern.match(contentSubString):
				
					choiceText, passageToLinkTo =  self.processChoiceString(contentSubString[2:(len(contentSubString)-2)])
					
					choiceData = {
						"type": self.passageTextType.choiceText,
						"choiceText": choiceText,
						"passageToLinkTo": passageToLinkTo,
						"tweetData": None
					}
					
					contentData.append(choiceData)
					
				else:
					# each 'paragraph' (with 2 or more newlines) of passage text should be a separate tweet.
					# if just one newline, keep it and include in the tweet text
					paragraphBreakPattern = re.compile("\n{2,}")
					passageTextParagraphs = paragraphBreakPattern.split(contentSubString)
					
					for passageTextParagraph in passageTextParagraphs:
						if passageTextParagraph:
							passageData = {
								"type": self.passageTextType.passageText,
								"passageText": passageTextParagraph,
								"tweetData": None
							}
							
							contentData.append(passageData)

		return contentData

	def processPassagedataHtml(self, divHeadingsString, contentStr):

		passageData = {
			"pid": self.getPID(divHeadingsString),
			"name": self.getPassageName(divHeadingsString),
			"tags": self.getTags(divHeadingsString),
			"headerTweetData": None,
			"contentData": self.getContentData(contentStr)
		}
		
		return passageData

	# takes the choice string (whatever is inside the [[ ]]) and returns choiceText, passageToLinkTo
	# TODO: multiple arrows in the string not supported. support it or validate against it
	def processChoiceString(self, choiceString):
		rightArrowPattern = re.compile("->")
		leftArrowPattern = re.compile("<-")
		pipePattern = re.compile("\|")
		
		# try splitting the string at "->"
		rightArrowSplitString = rightArrowPattern.split(choiceString)
		if len(rightArrowSplitString) > 1:
			return rightArrowSplitString[0], rightArrowSplitString[1]
		
		# try splitting the string at "<-"
		leftArrowSplitString = leftArrowPattern.split(choiceString)
		if len(leftArrowSplitString) > 1:
			return leftArrowSplitString[1], leftArrowSplitString[0]
		
		# try splitting at "|"
		pipeSplitString = pipePattern.split(choiceString)
		if len(pipeSplitString) > 1:
			return pipeSplitString[0], pipeSplitString[1]
		
		# no arrows so choice text is both the text displayed and passage linked to
		return choiceString, choiceString

	def removeAllOccurencesFromString(self, subStrToRemove, stringToRemoveFrom):
		toRemovePattern = re.compile(subStrToRemove)
		return toRemovePattern.sub("", stringToRemoveFrom)

	def getHtmlSplitByPassageDataDiv(self):
		storydataHtml = self.importTwineHtmlFile()

		storydataHtml = self.removeAllOccurencesFromString("<\/tw-passagedata>", storydataHtml)
		storydataHtml = self.removeAllOccurencesFromString("<\/tw-storydata>", storydataHtml)

		passageDataPattern = re.compile("(<tw-passagedata.+?>)")
		return passageDataPattern.split(storydataHtml)

	def extractPassageDataFromFile(self):
		htmlSplitByPassageDataDiv = self.getHtmlSplitByPassageDataDiv()

		storydataDivHeadings = htmlSplitByPassageDataDiv[0]
		startnode = self.getStartNode(storydataDivHeadings)

		passageDataList = list()
		for i in range(0, int(len(htmlSplitByPassageDataDiv)/2)):
			passageDataList.append(self.processPassagedataHtml(htmlSplitByPassageDataDiv[2*i + 1], htmlSplitByPassageDataDiv[2*i + 2]))
		
		return startnode, passageDataList
