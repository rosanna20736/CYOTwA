
# up to 5 lines of (not including \n):
# up to 38 chars using regular chars (though width is inconsistent)
# up to 23 chars using block elements (https://fsymbols.com/draw/)
# up to 16 using fullwidth text (http://xahlee.info/comp/unicode_full-width_chars.html) (make sure whitespace/symbols are fullwidth)

def getTweetHeader(passageName, asciiArtEnabled, tagName = "default"):
# this constructs every element in the dictionary for every single passage.
# TODO: get a way that this isn't necessary and also is more user friendly to define new headers
	asciiArtTweetHeaders = {

		"default":
		(
		"--------------------------------------"	+ "\n" +
		"--------------------------------------"	+ "\n" +
		passageName.center(38, '-')					+ "\n" +
		"--------------------------------------"	+ "\n" +
		"--------------------------------------"	+ "\n"
		)

	}
	
	asciiArtFreeTweetHeaders = {
		
		"default": passageName
		
	}
	
	headersToUse = asciiArtTweetHeaders if asciiArtEnabled else asciiArtFreeTweetHeaders
	
	if tagName in headersToUse:
		return headersToUse[tagName]
	
	return headersToUse["default"]