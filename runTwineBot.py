# import Tkinter
# import tkFileDialog

import twineParser
import tweetingFunctions as tweet
import tweetHeaders

class twineTwitterBot:
	
	def __init__(self, storyHtmlFileName, asciiArtEnabled = True):
		
		self.asciiArtEnabled = asciiArtEnabled
	
		self.passageTextType = twineParser.passageTextTypeClass()

		myTwineParser = twineParser.twineParser(storyHtmlFileName)
		self.startnode, self.passageDataList = myTwineParser.extractPassageDataFromFile()	
	
	def getPassageFromPassageName(self, name):
		return list(filter(lambda passage: passage["name"] == name, self.passageDataList))[0]

	def tweetStory(self):
		# TODO: split into a couple of separate functions
		self.printTweetCount()
		
		# tweet passage headers
		for passageData in self.passageDataList:
			# TODO: search through all tags for one that matches a header
			if not passageData["headerTweetData"]:
				if passageData["tags"]:
					headerText = tweetHeaders.getTweetHeader(passageData["name"], self.asciiArtEnabled, passageData["tags"][0])
				else:
					headerText = tweetHeaders.getTweetHeader(passageData["name"], self.asciiArtEnabled)
					
				try:
					passageData["headerTweetData"] = tweet.postTweet(headerText)
				except tweet.TweetError:
					self.deleteAllTweets()
					print("Error tweeting header tweet for passage: " + passageData["name"])
					raise

		# tweet all passage and choice text, attaching to the correspoding headers
		for passageData in self.passageDataList:
			for i in range(0, len(passageData["contentData"])):
				contentDataEntry = passageData["contentData"][i]
				
				if not contentDataEntry["tweetData"]:
				
					if i == 0:
						tweetToReplyTo = passageData["headerTweetData"]
					else:
						tweetToReplyTo = passageData["contentData"][i - 1]["tweetData"]
					
					try:
						if contentDataEntry["type"] == self.passageTextType.passageText:
							contentDataEntry["tweetData"] = tweet.replyToTweet(contentDataEntry["passageText"], tweetToReplyTo)
						elif contentDataEntry["type"] == self.passageTextType.choiceText:
							passageToLinkTo = self.getPassageFromPassageName(contentDataEntry["passageToLinkTo"])
							contentDataEntry["tweetData"] = tweet.replyToTweet(contentDataEntry["choiceText"],tweetToReplyTo, passageToLinkTo["headerTweetData"])
					except tweet.TweetError:
						self.deleteAllTweets()
						print("Error tweeting content for passage: " + passageData["name"] + " with contentData: " + str(contentDataEntry))
						raise
					
	def deleteAllTweets(self):
		for passageData in self.passageDataList:
		
			if passageData["headerTweetData"]:
				tweet.deleteTweet(passageData["headerTweetData"])
				
			for contentData in passageData["contentData"]:
				if contentData["tweetData"]:
					tweet.deleteTweet(contentData["tweetData"])
					
	def getStartTweet(self):
		startingPassage = list(filter(lambda passage: passage["pid"] == 1, self.passageDataList))[0]
		return startingPassage["headerTweetData"]
		
	def printTweetCount(self):
		tweetCount = len(self.passageDataList)
		for passageData in self.passageDataList:
			tweetCount = tweetCount + len(passageData["contentData"])
		
		print("Number of tweets required for this story: " + str(tweetCount))

# TODO: for debugging - delete this eventually
def delete():
	myTwineBot.deleteAllTweets()
	
##### EXECUTE SCRIPT #####

useHardCodedFilePath = True
if useHardCodedFilePath:
	# storyHtmlFileName = "C:\Users\Rosanna\Documents\Twine\Stories\Test.html"
	storyHtmlFileName = "C:\\Users\\Rosanna\\Documents\\Twine\\Stories\\Twitter CYOA.html"
else:
	Tkinter.Tk().withdraw()
	storyHtmlFileName = tkFileDialog.askopenfilename(initialdir="C:\\Users\\", title="Select story HTML file",
													 filetypes=(("HTML files", "*.html"), ("all files", "*.*")))

# TODO: save twineTwitterBot instance
myTwineBot = twineTwitterBot(storyHtmlFileName, True)
myTwineBot.tweetStory()
startingTweet = myTwineBot.getStartTweet()

print("Starting tweet: " + tweet.getTweetLink(startingTweet))